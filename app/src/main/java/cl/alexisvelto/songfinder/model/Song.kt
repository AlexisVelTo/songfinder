package cl.alexisvelto.songfinder.model

data class Song(val songName:String, val collectionName:String, val collectionImage:String,val collectionId:String,val artistName:String)