package cl.alexisvelto.songfinder.model.repository

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import cl.alexisvelto.songfinder.model.Song
import cl.alexisvelto.songfinder.util.JsonFormatter
import cl.alexisvelto.songfinder.util.SingletonHolder
import com.google.gson.JsonObject
import kotlin.collections.ArrayList
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SongRepository private constructor(context: Context){
    var context = context
    companion object : SingletonHolder<SongRepository, Context>(::SongRepository)

    fun getSongs(query:String, msongs:MutableLiveData<ArrayList<Song>>): Observable<ArrayList<Song>> {
        val retrofit1 = Retrofit.Builder()
            .baseUrl("http://itunes.apple.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service1 = retrofit1.create<APIService>(APIService::class.java)
        val jsonCall = service1.getJson(query)

        jsonCall.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                var jsonarray = response.body()?.getAsJsonArray("results")
                println(response.code())
                println(response.body())
                var songs = ArrayList<Song>()
                jsonarray?.forEach {
                    songs.add(JsonFormatter.format(it))
                }
                msongs.value = songs
                //TODO persist data
            }
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.e("SONG_SEARCHER", t.toString())
                Toast.makeText(context, "Error, please try again", Toast.LENGTH_LONG).show()
            }
        })

        return Observable.just(ArrayList<Song>())
    }
}