package cl.alexisvelto.songfinder.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cl.alexisvelto.songfinder.R
import cl.alexisvelto.songfinder.viewModel.AlbumViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_album.*

class AlbumActivity:AppCompatActivity() {
    lateinit var mainViewModel:AlbumViewModel
    private lateinit var songAapter: AlbumSongAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var collectionId:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_album)

        var collectionUrl = getIntent().getExtras().getString("collectionImageUrl")
        var collectionName = getIntent().getExtras().getString("collectionName")
        var artistName = getIntent().getExtras().getString("artistName")
        collectionId = getIntent().getExtras().getString("collectionId")

        Picasso.get().load(collectionUrl).into(imageViewCollection)

        textViewCollectionName.text = collectionName
        textViewArtistName.text = artistName

        mainViewModel = ViewModelProviders.of(this).get(AlbumViewModel::class.java)
        mainViewModel.songs.observe(this, Observer {
            songAapter.replaceData(it)
        })

        recyclerView = findViewById<RecyclerView>(R.id.recyclerview_album_songs)
        songAapter = AlbumSongAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = songAapter
    }

    override fun onResume() {
        super.onResume()
        mainViewModel.loagSongs("https://itunes.apple.com/lookup?id=$collectionId&entity=song")
    }
}