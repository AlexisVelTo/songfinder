package cl.alexisvelto.songfinder.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cl.alexisvelto.songfinder.R
import cl.alexisvelto.songfinder.model.Song

class AlbumSongAdapter(context: Context) : RecyclerView.Adapter<AlbumSongAdapter.AlbumSongViewHolder>(){
    private var context = context
    private var songList = ArrayList<Song>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumSongViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_album_song, parent, false)
        return AlbumSongViewHolder(view)
    }

    override fun getItemCount(): Int {
        return songList.size
    }

    override fun onBindViewHolder(holder: AlbumSongViewHolder, position: Int) {
        val song = songList[position]
        holder.songName.text = song.songName
    }

    fun replaceData(newData: ArrayList<Song>){
        songList = newData
        notifyDataSetChanged()
    }

    class AlbumSongViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var songName: TextView = itemView.findViewById(R.id.album_song)
    }

}