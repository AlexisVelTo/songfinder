package cl.alexisvelto.songfinder.view

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.lifecycle.Observer
import cl.alexisvelto.songfinder.R
import cl.alexisvelto.songfinder.viewModel.MainViewModel


class MainActivity : AppCompatActivity() {
    lateinit var mainViewModel:MainViewModel
    private lateinit var songAapter: SongAdapter
    private lateinit var mSearchView: SearchView
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        mainViewModel.songs.observe(this,Observer {
            songAapter.replaceData(it)
        })
        recyclerView = findViewById<RecyclerView>(R.id.recyclerview_songs)
        songAapter = SongAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = songAapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        mSearchView = menu?.findItem(R.id.action_search)?.actionView as SearchView
        mSearchView.maxWidth = Integer.MAX_VALUE
        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                mainViewModel.loagSongs("/search?term=$query&mediaType=music&limit=20")
                return false
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
        return true
    }
}
