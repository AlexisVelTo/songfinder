package cl.alexisvelto.songfinder.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import cl.alexisvelto.songfinder.R
import cl.alexisvelto.songfinder.model.Song


class SongAdapter(context:Context) : RecyclerView.Adapter<SongAdapter.SongViewHolder>(){
    private var context = context
    private var songList = ArrayList<Song>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_song, parent, false)
        return SongViewHolder(view)
    }

    fun replaceData(newData: ArrayList<Song>){
        songList = newData
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
       return songList.size
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        val song = songList[position]
        holder.songName.text = song.songName
        holder.songArtist.text = song.artistName
        holder.songItem.setOnClickListener {
            val intent = Intent(context, AlbumActivity::class.java)
            val mBundle = Bundle()
            mBundle.putString("collectionName", song.collectionName)
            mBundle.putString("songName", song.songName)
            mBundle.putString("collectionImageUrl", song.collectionImage)
            mBundle.putString("collectionId", song.collectionId)
            mBundle.putString("artistName", song.artistName)
            intent.putExtras(mBundle)
            context.startActivity(intent)
        }
    }

    class SongViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var songItem: CardView = itemView.findViewById(R.id.cardViewSongItem)
        var songName: TextView = itemView.findViewById(R.id.textViewSongName)
        var songArtist: TextView = itemView.findViewById(R.id.textViewSongArtist)
    }

}