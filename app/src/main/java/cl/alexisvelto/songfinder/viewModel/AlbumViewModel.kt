package cl.alexisvelto.songfinder.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import cl.alexisvelto.songfinder.model.Song
import cl.alexisvelto.songfinder.model.repository.SongRepository

class AlbumViewModel (application: Application): AndroidViewModel(application) {
    val songRepository = SongRepository.getInstance(application.applicationContext)
    val songs = MutableLiveData<ArrayList<Song>>()
    var isLoading = MutableLiveData<Boolean>()

    fun loagSongs(query:String){
        isLoading.value = true
/*        songRepository.getSongs(query).subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object: DisposableObserver<ArrayList<Song>>(){
                override fun onComplete() {
                    isLoading.value = false
                }
                override fun onNext(apps: ArrayList<Song>) {

                }
                override fun onError(e: Throwable) {

                }
            })*/
        songRepository.getSongs(query,songs)
    }
}